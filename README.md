# Demo Template

This repo is a template.  
Forking it will create a new repo with basic tools already present.  

## Updating child repos
If the template repo is updated child repos can pull in the new changes by 
running the below
```
task pull_template
```

Also if the child was forked from the template you could create a merge request
from the template repo that targets the child repo.

## Cleaning up
If a demo commit has been made it can be reset by running 
```
task reset
```
